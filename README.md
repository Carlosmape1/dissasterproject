# README   
#### Git is a develop tool, as your favorite IDE, use it in your daily life   
[Refference guide](https://www.digitalocean.com/community/cheatsheets/how-to-use-git-a-reference-guide)
## Topics to work with   
 - [ ] **Common git commands** [This Issue](#1)
    - [ ] **[add]**  Take changes to stagged files
    - [ ] **[commit]** Take stagged changes to commit (confirm stagged changes)
    - [ ] **[push/pull]** Synchronize branch HEAD pointer with local/remote repository
    - [ ] **[fetch]** Synchronize local/remote repositories (branches)
    - [ ] **[cherry-pick]** Take a commit to another branch
    - [ ] **[revert]** Undone commited changes 
    - [ ] **[merge/rebase]** Merge commits from 2 different branches
 - [ ] **Common git BAD MANNERS** How to properly work with Git *(without dying in the attempt)*
    - [ ] **Large commits**  
    - [ ] **Non descriptive commits**  
    - [ ] **Use of local branches** just to avoid local chaos as nown as "It works in my PC"
 - [ ] **Common GitLab features**
    - [ ] **Branches** Easy view general status using (Graph)[https://gitlab.com/Carlosmape1/dissasterproject/-/network/master]
    - [ ] **Blame**  Look for a file concrete changes (Bad manner Blame)[https://gitlab.com/Carlosmape1/dissasterproject/-/blame/master/README.md] (Good manner Blame, see another branch & file)
    - [ ] **Merge Request**
    - [ ] **Integrated MR conflicts solver** [This MR](https://gitlab.com/Carlosmape1/dissasterproject/-/merge_requests/2)
